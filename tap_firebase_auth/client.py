"""Custom client handling, including FirebaseAuthStream base class."""

import requests
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable
from singer_sdk.plugin_base import PluginBase as TapBaseClass
from singer_sdk.streams import Stream
from singer.schema import Schema


class FirebaseAuthStream(Stream):
    """Stream class for FirebaseAuth streams."""

    def __init__(
        self,
        tap: TapBaseClass,
        name: Optional[str] = None,
        schema: Optional[Union[Dict[str, Any], Schema]] = None,
        auth: Any = None
    ) -> None:
        super().__init__(name=name, schema=schema, tap=tap)
        self.auth = auth
