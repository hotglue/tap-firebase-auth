"""Stream type classes for tap-firebase-auth."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable, Tuple

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_firebase_auth.client import FirebaseAuthStream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class UsersStream(FirebaseAuthStream):
    """Define custom stream."""
    name = "users"
    primary_keys = ["uid"]
    replication_key = None
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("uid", th.StringType),
        th.Property("custom_claims", th.StringType),
        th.Property("disabled", th.BooleanType),
        th.Property("display_name", th.StringType),
        th.Property("email", th.StringType),
        th.Property("email_verified", th.BooleanType),
        th.Property("password_hash", th.StringType),
        th.Property("password_salt", th.StringType),
        th.Property("phone_number", th.StringType),
        th.Property("photo_url", th.StringType),
        th.Property("provider_id", th.StringType),
        th.Property("tenant_id", th.StringType),
        th.Property("tokens_valid_after_timestamp", th.IntegerType),
    ).to_dict()

    def get_records(
        self, context: Optional[dict]
    ) -> Iterable[Union[dict, Tuple[dict, dict]]]:
        """Abstract row generator function. Must be overridden by the child class.

        Each row emitted should be a dictionary of property names to their values.
        Returns either a record dict or a tuple: (record_dict, child_context)

        A method which should retrieve data from the source and return records
        incrementally using the python `yield` operator.

        Only custom stream types need to define this method. REST and GraphQL streams
        should instead use the class-specific methods for REST or GraphQL, respectively.

        This method takes an optional `context` argument, which can be safely ignored
        unless the stream is a child stream or requires partitioning.
        More info: :doc:`/partitioning`.

        Parent streams can optionally return a tuple, in which
        case the second item in the tuple being a `child_context` dictionary for the
        stream's `context`.
        More info: :doc:`/parent_streams`

        Args:
            context: Stream partition or context dictionary.
        """
        records = []
        page = self.auth.list_users()
        while page:
            for user in page.users:
                #records.append(user.to_dict())
                #This is a ExportedUserRecord object direct casting functions like to_dict() don't work on it. 
                records.append({
                    "uid":user.uid,
                    "custom_claims": user.custom_claims,
                    "disabled": user.disabled,
                    "display_name": user.display_name,
                    "email": user.email,
                    "email_verified": user.email_verified,
                    "password_hash": user.password_hash,
                    "password_salt": user.password_salt,
                    "photo_url": user.photo_url,
                    "phone_number": user.phone_number,
                    "provider_id": user.provider_id,
                    "tenant_id": user.tenant_id,
                    "tokens_valid_after_timestamp": user.tokens_valid_after_timestamp,
                    })
            # Get next batch of users.
            page = page.get_next_page()

        return records
